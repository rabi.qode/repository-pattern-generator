# Repository Generator for laravel projects

Repository Generator is the folder structure generator which follows a repository pattern

# ### Installation:

  - Clone or Download this repo.
  - Copy a folder stub in resources folder.
  - create a command using php artisan make:command {command name }

```sh
$ php artisan make:command RepoGenerator
```
This will create a command file inside app/console/commands/RepoGenerator.

Copy the contents from repo and replace that file.

## Finally

```sh
$ php artisan repo:generator User
```

Repositories are created inside the Model Folder.

Enjoy !!!!
